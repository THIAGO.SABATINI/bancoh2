/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atividade.segunda.bancodedados.restapi;

import atividade.segunda.bancodedados.dao.PessoaDAO;

import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.*;
import atividade.segunda.bancodedados.domain.Pessoa;

/**
 *
 * @author WilliamFernandoMende
 */
@RestController
public class PessoaApi {

    
    private PessoaDAO pessoaDAO= new PessoaDAO();
    
    @GetMapping("/pessoas")
    public List<Pessoa> obterPessoas(){
        return pessoaDAO.findAll();
    }

    @RequestMapping(value = "id/{id}", method = RequestMethod.GET)
    public Pessoa obterPorId(@PathVariable(value= "id") Integer id) throws Exception {
        return pessoaDAO.getPessoaId(id);
    }

    @RequestMapping(value = "nome/{nome}", method = RequestMethod.GET)
    public List<Pessoa> obterPorNome(@PathVariable(value = "nome") String nome) throws Exception{
        return pessoaDAO.getPessoaNome(nome);
    }

    @RequestMapping(value = "data/{ano}", method = RequestMethod.GET)
    public List<Pessoa> obterPorData(@PathVariable(value = "ano") int ano)throws Exception{
        return pessoaDAO.getPessoasData(ano);
    }


    
}
